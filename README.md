Graylog Network Monitoring Practice with Wordpress
==================================================

These ansible scripts will create two AWS EC2 intances - a graylog instance to collect logs and a wordpress instance to send logs.
This is intentionally not completely secure and can use improvements. That's your job!

The graylog instance is a t2.medium and the wordpress instance is a t2.nano

This was tested from Ubuntu 18.04 with the following packages installed:
python3-boto3 pwgen python-boto3 python-boto ansible

* FIRST THING: EDIT 'vars.sh'

You must put a valid AWS access and secret token in this file with permissions to add and edit security groups, ssh keys and EC2 instances.
The default region is us-west-1 with an Ubuntu 18.04 AMI. This can be changed as you see fit.
'EC2' and 'Full Access' should work for AWS credentials.
You REALLY SHOULD put your IP address under MY_IP as it will by default only allow this IP access to these instances.
When you're ready to allow anyone to access wordpress, add the 'graylog-practice-open' security group to it. This is intentionally not
automatic as you must first configure wordpress and add a couple inputs to graylog.

* Run "./init.sh" to generate a key pair, hash your graylog password and perform most of the installation and configuration.
Note that it will only ask for your desired graylog password once. Get it right.

You might see some '[ERROR]:' statements with nothing else next to them. This is a known problem with an older version of ansible but
doesn't cause any issues running it.

* Navigate to your wordpress site at http://(SOME IP) and fill out the installation questions.

* Make sure you can log in to your graylog site at http://(SOME IP) and 
** add a Syslog UDP input on port 1515
** add a Netflow UDP input on port 2055

* Use whichever means you want to set 'graylog-practice-open' security group on your wordpress instance only
** This will then allow anyone to access port 80.

It is then your job to practice monitoring the wordpress site and add security improvements as you see fit.
Some ideas:
* Add firehol.org level 1,2,3 blacklists to an ipset on an iptables firewall
* Add fail2ban to block some brute force attacks
* Add a WAF to block more attacks
* Use iptables limit rules to prevent DoS and some brute force attacks
* Probably a 100 other possibilities!

When done, run './destroy.sh' to remmove all of the AWS resources that were created.


