#!/bin/bash
if [ ! -f graylog -o ! -f graylog.pub ]; then
  ssh-keygen -b 4096 -f ./graylog
fi
if [ ! -r graylog.pw ]; then
  echo "Enter graylog password:"
  read -s GLP
  echo $GLP | tr -d '\n' | sha256sum | cut -d" " -f1 > graylog.pw
fi
ssh-add ./graylog
source ./vars.sh
ansible-playbook -i localhost, ./init.yml
ansible-playbook -i ec2.py ./config.yml
ansible-playbook -i ec2.py ./config_web.yml &
ansible-playbook -i ec2.py ./config_log.yml

